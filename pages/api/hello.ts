// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { Client } from "@notionhq/client";

const notion = new Client({ auth: process.env.NOTION_KEY });

type Data = {
  name: string;
};
const databaseId = process.env.NOTION_DATABASE_ID;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  // const databaseId = '668d797c-76fa-4934-9b05-ad288df2d136';
  const response = await notion.databases.retrieve({
    database_id: databaseId || "",
  });
  console.log(response);
  res.status(200).json({ response });
}
